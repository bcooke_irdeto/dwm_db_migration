#!/usr/bin/python

# -*- coding: utf-8 -*-

import argparse
import psycopg2
import cx_Oracle
import hashlib
import sys


parser = argparse.ArgumentParser()
parser.add_argument('--host-from', dest='hostname_from', default='localhost')
parser.add_argument('--host-to', dest='hostname_to', default='localhost')
parser.add_argument('--sid-to', dest='sid_to', default='orcl')
parser.add_argument('--db-from', dest='db_from', default='dwm')
parser.add_argument('--schema-to', dest='schema_to', default='dwm')
parser.add_argument('--username-from', dest='username_from', default='postgres')
parser.add_argument('--username-to', dest='username_to', default='dwm')
parser.add_argument('--password-from', dest='password_from', default='postgres')
parser.add_argument('--password-to', dest='password_to', default='dwm123')
args = parser.parse_args()

print args.hostname_from
delete_confirm = """IMPORTANT: Double-check that you are migrating to a dwm database with no
records in the tables.  Any existing records will be deleted by this script.

We will be reading from a PostgreSQL database called %s 
on host %s, username %s, and migrating data to an empty 
or safe to delete records) schema called %s on host %s,
sid %s, username %s.

Confirm and proceed by typing out "yes" to confirm you wish to proceed: """ % (
    args.db_from,
    args.hostname_from,
    args.username_from,
    args.schema_to,
    args.hostname_to,
    args.sid_to,
    args.username_to
)
response = raw_input(delete_confirm)
if response != "yes":
    sys.exit()

try:
    con_old = psycopg2.connect(
        host = args.hostname_from,
        database = 'dwm',
        user = args.username_from,
        password = args.password_from
    )
    conn_string_new = "%s/%s@%s/%s" % (
        args.username_to,
        args.password_to,
        args.hostname_to,
        args.sid_to
    )
    con_new = cx_Oracle.connect(conn_string_new)

    cur_old = con_old.cursor()
    cur_new = con_new.cursor()

    # Clear out the tables to be inserted

    print "Emptying destination tables"
    cur_new.execute("DELETE FROM activemq_acks")
    cur_new.execute("DELETE FROM activemq_lock")
    cur_new.execute("DELETE FROM activemq_msgs")

    cur_new.execute("DELETE FROM transfer_job")
    cur_new.execute("DELETE FROM distributor")

    cur_new.execute("DELETE FROM tmid")
    cur_new.execute("DELETE FROM wm_embeddings");
    cur_new.execute("DELETE FROM wm_job")
    cur_new.execute("DELETE FROM wm_agent")

    cur_new.execute("DELETE FROM wm_embedding_parameters")
    cur_new.execute("DELETE FROM wm_job_cancel")

    # Dictionaries to hold SQL queries
    sql_old   = {}
    sql_new   = {}
    sql_old_check = {}
    sql_new_check = {}

    # activemq_acks
    sql_old['activemq_acks'] = """SELECT
  container,
  sub_dest,
  client_id,
  sub_name,
  selector,
  last_acked_id,
  priority,
  xid
FROM activemq_acks
ORDER BY container, client_id, sub_name, priority"""
    sql_new['activemq_acks'] = """INSERT INTO activemq_acks (
  container,
  sub_dest,
  client_id,
  sub_name,
  selector,
  last_acked_id,
  priority,
  xid
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8
)
"""
    sql_old_check['activemq_acks'] = sql_old['activemq_acks']
    sql_new_check['activemq_acks'] = sql_old['activemq_acks']

    # activemq_lock
    sql_old['activemq_lock'] = """SELECT
  id,
  time,
  broker_name
FROM activemq_lock
ORDER BY id"""
    sql_new['activemq_lock'] = """INSERT INTO activemq_lock (
  id,
  time,
  broker_name
)
VALUES (
  :1,
  :2,
  :3
)
"""
    sql_old_check['activemq_lock'] = sql_old['activemq_lock']
    sql_new_check['activemq_lock'] = sql_old['activemq_lock']

    # activemq_msgs
    sql_old['activemq_msgs'] = """SELECT
  id,
  container,
  msgid_prod,
  msgid_seq,
  expiration,
  msg,
  priority,
  xid
FROM activemq_msgs
ORDER BY id"""
    sql_new['activemq_msgs'] = """INSERT INTO activemq_msgs (
  id,
  container,
  msgid_prod,
  msgid_seq,
  expiration,
  msg,
  priority,
  xid
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8
)
"""
    sql_old_check['activemq_msgs'] = """SELECT
  id,
  container,
  msgid_prod,
  msgid_seq,
  expiration,
  msg,
  priority,
  CASE WHEN xid = '' THEN NULL ELSE xid END AS xid
FROM activemq_msgs
ORDER BY id"""
    sql_new_check['activemq_msgs'] = sql_old['activemq_msgs']

    # distributor
    sql_old['distributor'] = """SELECT
  id,
  distributor_id,
  distributor_name
FROM distributor
ORDER BY id"""
    sql_new['distributor'] = """INSERT INTO distributor (
  id,
  distributor_id,
  distributor_name
)
VALUES (
  :1,
  :2,
  :3
)
"""
    sql_old_check['distributor'] = sql_old['distributor']
    sql_new_check['distributor'] = sql_old['distributor']

    # tmid
    sql_old['tmid'] = """SELECT
  id,
  asset_id,
  distributor_id,
  distributor_name,
  title_id,
  tmid,
  operator_id,
  sync_status,
  last_sync_try,
  opd_id
FROM tmid
ORDER BY id"""
    sql_new['tmid'] = """INSERT INTO tmid (
  id,
  asset_id,
  distributor_id,
  distributor_name,
  title_id,
  tmid,
  operator_id,
  sync_status,
  last_sync_try,
  opd_id
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8,
  :9,
  :10
)
"""
    sql_old_check['tmid'] = """SELECT
  id,
  asset_id,
  distributor_id,
  distributor_name,
  title_id,
  tmid,
  operator_id,
  sync_status,
  DATE_TRUNC('second', last_sync_try) AS last_sync_try,
  opd_id
FROM tmid
ORDER BY id"""
    sql_new_check['tmid'] = sql_old['tmid']

    # transfer_job
    sql_old['transfer_job'] = """SELECT
  id,
  priority,
  wm_job_id,
  distributor_id,
  output_file,
  status,
  error_code,
  error_message,
  submission_time,
  start_time,
  complete_time,
  tmid
FROM transfer_job
ORDER BY id"""
    sql_new['transfer_job'] = """INSERT INTO transfer_job (
  id,
  priority,
  wm_job_id,
  distributor_id,
  output_file,
  status,
  error_code,
  error_message,
  submission_time,
  start_time,
  complete_time,
  tmid
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8,
  :9,
  :10,
  :11,
  :12
)
"""
    sql_old_check['transfer_job'] = """SELECT
  id,
  priority,
  wm_job_id,
  distributor_id,
  output_file,
  status,
  error_code,
  error_message,
  DATE_TRUNC('second', submission_time) AS submission_time,
  DATE_TRUNC('second', start_time) AS start_time,
  DATE_TRUNC('second', complete_time) AS complete_time,
  tmid
FROM transfer_job
ORDER BY id"""
    sql_new_check['transfer_job'] = sql_old['transfer_job']

    # wm_agent
    sql_old['wm_agent'] = """SELECT
  id,
  hostname,
  status,
  process_id,
  heartbeat,
  uptime,
  agent_id
FROM wm_agent
ORDER BY id"""
    sql_new['wm_agent'] = """INSERT INTO wm_agent (
  id,
  hostname,
  status,
  process_id,
  heartbeat,
  uptime,
  agent_id
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7
)
"""
    sql_old_check['wm_agent'] = """SELECT
  id,
  hostname,
  status,
  process_id,
  DATE_TRUNC('second', heartbeat) AS heartbeat,
  uptime,
  agent_id
FROM wm_agent
ORDER BY id"""
    sql_new_check['wm_agent'] = sql_old['wm_agent']

    # wm_embedding_parameters
    sql_old['wm_embedding_parameters'] = """SELECT
  id,
  method,
  strength,
  wm_interval,
  sc_psnr_target,
  sc_max_strength,
  non_sc_psnr_target,
  non_sc_max_strength,
  min_strength,
  strength_step,
  operator_id,
  min_bitrate,
  max_bitrate
FROM wm_embedding_parameters
ORDER BY id"""
    sql_new['wm_embedding_parameters'] = """INSERT INTO wm_embedding_parameters (
  id,
  method,
  strength,
  wm_interval,
  sc_psnr_target,
  sc_max_strength,
  non_sc_psnr_target,
  non_sc_max_strength,
  min_strength,
  strength_step,
  operator_id,
  min_bitrate,
  max_bitrate
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8,
  :9,
  :10,
  :11,
  :12,
  :13
)
"""
    sql_old_check['wm_embedding_parameters'] = sql_old['wm_embedding_parameters']
    sql_new_check['wm_embedding_parameters'] = sql_old['wm_embedding_parameters']

    # wm_embeddings
    sql_old['wm_embeddings'] = """SELECT
  id,
  wm_job_id,
  frame_timestamp,
  strength,
  psnr,
  result,
  selection_type
FROM wm_embeddings
ORDER BY id"""
    sql_new['wm_embeddings'] = """INSERT INTO wm_embeddings (
  id,
  wm_job_id,
  frame_timestamp,
  strength,
  psnr,
  result,
  selection_type
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7
)
"""
    sql_old_check['wm_embeddings'] = sql_old['wm_embeddings']
    sql_new_check['wm_embeddings'] = """SELECT
  id,
  wm_job_id,
  frame_timestamp,
  strength,
  CASE WHEN psnr = 'Infinity' THEN psnr ELSE ROUND(psnr, 4) END AS psnr,
  result,
  selection_type
FROM wm_embeddings
ORDER BY id"""

    # wm_job
    sql_old['wm_job'] = """SELECT
  id,
  asset_id,
  title_id,
  in_file,
  priority,
  status,
  progress,
  error_code,
  error_message,
  variant_file_path,
  process_id,
  agent_id,
  submission_time,
  start_time,
  complete_time,
  wm_embedding_parameters
FROM wm_job
ORDER BY id"""
    sql_new['wm_job'] = """INSERT INTO wm_job (
  id,
  asset_id,
  title_id,
  in_file,
  priority,
  status,
  progress,
  error_code,
  error_message,
  variant_file_path,
  process_id,
  agent_id,
  submission_time,
  start_time,
  complete_time,
  wm_embedding_parameters
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6,
  :7,
  :8,
  :9,
  :10,
  :11,
  :12,
  :13,
  :14,
  :15,
  :16
)
"""
    sql_old_check['wm_job'] = """SELECT
  id,
  asset_id,
  title_id,
  in_file,
  priority,
  status,
  progress,
  error_code,
  error_message,
  variant_file_path,
  process_id,
  agent_id,
  DATE_TRUNC('second', submission_time) AS submission_time,
  DATE_TRUNC('second', start_time) AS start_time,
  DATE_TRUNC('second', complete_time) AS complete_time,
  wm_embedding_parameters
FROM wm_job
ORDER BY id"""
    sql_new_check['wm_job'] = sql_old['wm_job']

    # wm_job_cancel
    sql_old['wm_job_cancel'] = """SELECT
  id,
  wm_job_id,
  variant_file_path,
  agent_id,
  cancel_status,
  num_cancel_resend
FROM wm_job_cancel
ORDER BY id"""
    sql_new['wm_job_cancel'] = """INSERT INTO wm_job_cancel (
  id,
  wm_job_id,
  variant_file_path,
  agent_id,
  cancel_status,
  num_cancel_resend
)
VALUES (
  :1,
  :2,
  :3,
  :4,
  :5,
  :6
)
"""
    sql_old_check['wm_job_cancel'] = sql_old['wm_job_cancel']
    sql_new_check['wm_job_cancel'] = sql_old['wm_job_cancel']

    table_names = [
        'wm_job_cancel',
        'wm_embedding_parameters',

        'wm_agent',
        'wm_job',
        'wm_embeddings',
        'tmid',

        'distributor',
        'transfer_job',

        'activemq_msgs',
        'activemq_lock',
        'activemq_acks',
    ]

    md5_different = 0

    for table_name in table_names:
        # Load the new table from the old one
        print "Migrating %s" % table_name
        cur_old.execute(sql_old[table_name])
        rows_old = cur_old.fetchall()
        for row_old in rows_old:
            cur_new.execute(sql_new[table_name], row_old)

        # Compare MD5 hashes
        cur_old.execute(sql_old_check[table_name])
        cur_new.execute(sql_new_check[table_name])

        rows_old = cur_old.fetchall()
        for row_old in rows_old:
            row_new = cur_new.fetchone()

            rowstr_old = '-'.join(map(str, row_old))
            rowstr_new = '-'.join(map(str, row_new))
            md5_old = hashlib.md5(rowstr_old).hexdigest()
            md5_new = hashlib.md5(rowstr_new).hexdigest()

            if md5_old != md5_new:
                print "Old/new row mismatch:"
                print "Old: " + str(row_old)
                print "New: " + str(row_new)
                md5_different = 1

    if md5_different == 1:
        print "Row hash differences found.  Rolling back changes."
        con_new.rollback()

        sys.exit()

    # Reset sequences to maximum value of sequence field + 1

    sequence_tables = [
        'distributor',
        'tmid',
        'transfer_job',
        'wm_agent',
        'wm_embedding_parameters',
        'wm_embeddings',
        'wm_job_cancel',
        'wm_job',
    ]

    for sequence_table_name in sequence_tables:
        # Skip this table if it's empty
        cur_new.execute("SELECT COUNT(*) AS row_count FROM %s" % (sequence_table_name))
        rows_in_destination = cur_new.fetchone()[0]
        if rows_in_destination == 0:
            print "Table %s is empty. Skipping." % (sequence_table_name)
            continue

        print "Resetting sequence for table %s" % (sequence_table_name)
        # First, reset the sequence back to 1
        cur_new.execute("SELECT MAX(id) AS max_id FROM %s" % (sequence_table_name))
        max_sequenced_id_value = cur_new.fetchone()[0]

        cur_new.execute("SELECT %s_ID_SEQ.NEXTVAL FROM DUAL" % (sequence_table_name))
        next_sequence_value = cur_new.fetchone()[0]

        sql_reset_sequence_to_zero = "ALTER SEQUENCE %s_ID_SEQ INCREMENT BY -%s MINVALUE 0" % (sequence_table_name, next_sequence_value)
        cur_new.execute(sql_reset_sequence_to_zero)

        cur_new.execute("SELECT %s_ID_SEQ.NEXTVAL FROM DUAL" % (sequence_table_name))
        next_sequence_value = cur_new.fetchone()[0]

        sql_reset_sequence_to_one = "ALTER SEQUENCE %s_ID_SEQ INCREMENT BY 1 MINVALUE 0" % (sequence_table_name)
        cur_new.execute(sql_reset_sequence_to_one)

        # Then, compute an interval and reset sequence to the highest sequenced
        # column value, at 
        sql_reset_sequence_to_highest_column_value = "ALTER SEQUENCE %s_ID_SEQ INCREMENT BY %s" % (sequence_table_name, (max_sequenced_id_value))
        cur_new.execute(sql_reset_sequence_to_highest_column_value)
        cur_new.execute("SELECT %s_ID_SEQ.NEXTVAL FROM DUAL" % (sequence_table_name))
        cur_new.execute("ALTER SEQUENCE %s_ID_SEQ INCREMENT BY 1" % (sequence_table_name))

    
    print "Data checks complete. Committing changes."
    con_new.commit()


except psycopg2.DatabaseError, e:

    print 'Error %s' % e
    sys.exit(1)

except cx_Oracle.DatabaseError, e:

    print 'Error %s' % e
    sys.exit(1)

